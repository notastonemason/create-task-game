var express = require('express');
var app = express();
var http = require('http');
var path = require('path');
var reload = require()
var bodyParser = require('body-parser');
var logger = require('morgan');
app = express()
var publicDir = path.join(__dirname, 'public');
app.set('port', process.env.PORT || 3000);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/Scripts'));
app.get('/', function (req, res) {
  res.sendFile(path.join(publicDir, 'index.html'));
});
var server = http.createServer(app);
// Reload code here
reload(app).then(function (reloadReturned) {
  // reloadReturned is documented in the returns API in the README
  // Reload started, start web server
  server.listen(app.get('port'), function () {
    console.log('Web server listening on port ' + app.get('port'))
  })
}).catch(function (err) {
  console.error('Reload could not start, could not start server/sample app', err)
})