
For Windows 

Download and install nvm which is a Node Manager ( https://github.com/coreybutler/nvm-windows/releases )

Open a Command Window and type 'nvm install 12.16.2' which should install that version of Node. 

Once it is done you can check to make sure you have npm and Node installed by opening a new Command Window and typing 'Node -v' and 'npm -version' to make sure they are installed.

Download and install Node and npm ( https://docs.npmjs.com/downloading-and-installing-node-js-and-npm ) 

Unzip the file wherever you want 

Open a Command Prompt and navigate to that folder and type 'npm i' to install the Node Packages required
 
Once the node packages are downloaded type 'Node app' to start the server
 
Now open your browser and go to 'localhost:3000' 
