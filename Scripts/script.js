var c = document.getElementById("canvas1");
var ctx = c.getContext("2d");
ctx.lineWidth=4;
var boxSize = 40;
var boxSpace = 8;
boxSize+=boxSpace;
var background="#222222";
var time=0;
var shift = false;
var mapNum = 0;
var hitCount = 0;
var score = 0;
var devMode = false;
var mode = 0; //0=normal 1=targeting 2=map making'

const dir = {
  UP:1,
  RIGHT:2,
  DOWN:3,
  LEFT:4
}

const modeType = {
  NORMAL:0,
  TARGET:1,
  EDIT:2
}

const cellType = {
  WALL:0,
  FLOOR:1,
  DOOR:2,
  LOAD:3,
  GOLD:4
}

const properties = {
  CLOSED:0,
  OPEN:1
}

const palette = {
  0:background,
  1:"#888888",
  2:"#996622",
  3:"#7777AA",
  4:"#888888"
}

const statusPalette = {
  0:background,//wall
  1:"#CCCCCC",//floor
  2:"#bb8000",//door closed
  3:"#8888cc",//load
  4:"#D4AE37"//gold
}

const moveable = {
  0:false,
  1:true,
  2:false,
  3:true,
  4:false
}

const spells = {
  NONE:0,
  LIFE:1,
  WATER:2,
  ICE:3,
  EARTH:4,
  AIR:6,
  LAVA:7,
  FIRE:8,
  FIREBALL:9
}

const spellColors = {
  0:background,
  1:"#00FF00",
  2:"#0000FF",
  3:"#66AAFF",
  4:"#AA8800",
  6:"#B3FFFF",
  7:"#CC3000",
  8:"#FF0000",
  9:"#FF6666"
}

const statusList = {
  NONE:0,
  WET:1,
  FROZEN:2,
  FLAMING:3,
  STUCK:4
}

const durationList = {
  WET:12,
  FROZEN:8,
  FLAMING:5, 
  STUCK:10
}

const statusColors = {
  0:0,
  1:"#3366FF",
  2:"#99CCFF",
  3:"#FF6600",
  4:"#CC8800"
}

const mapList = ["map1","map_halls"];
var spell = spells.NONE;
var player = {x:2,y:2,dir:0,health:5,maxHealth:5,status:statusList.NONE,duration:0,tempX:2,tempY:1,borderColor:"#008800",fillColor:"#00CC00"}//dir up=1 right=2 down=3 left=4
var enemies = [{x:11,y:3,dir:0,health:8,maxHealth:8,status:statusList.NONE,duration:0,tempX:2,tempY:1,borderColor:"#880000",fillColor:"#CC0000"},
  {x:11,y:4,dir:0,health:10,maxHealth:10,status:statusList.NONE,duration:0,tempX:2,tempY:1,borderColor:"#880000",fillColor:"#880000"},
  {x:10,y:4,dir:0,health:8,maxHealth:8,status:statusList.NONE,duration:0,tempX:2,tempY:1,borderColor:"#880000",fillColor:"#CC0000"}]
var map = [[{cType:cellType.FLOOR},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.DOOR,state:properties.OPEN},{cType:cellType.DOOR,state:properties.OPEN},{cType:cellType.FLOOR},{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.DOOR,state:properties.OPEN},{cType:cellType.FLOOR},{cType:cellType.DOOR,state:properties.CLOSED},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}],[{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.WALL},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR},{cType:cellType.FLOOR}]]
loadMap("map1");

function strToArr(string){
  var tempFunc = Function('return '+string)
  return tempFunc()
}
async function loadMap(mapName){
  await fetch("./Maps/"+mapName+".txt")
  .then((resp)=>{          //same as function(){
    return resp.text();
  })
  .then((data)=>{
    data = data.split("\n");
    //console.log(data[0]);
    if(map[player.y][player.x].cType==cellType.LOAD){
      player.tempX=map[player.y][player.x].startX;
      player.y=map[player.y][player.x].startY;
      player.x=player.tempX;
    }
    map = strToArr(data[0]);
    enemies=strToArr(data.slice(1,data.length).join(""));
    drawMap();
  });
}

drawMap();
document.addEventListener('keydown', input);
document.addEventListener('keyup', inputUp);
function inputUp(event){
  if (event.code=="ShiftLeft"){
    shift = false;
  }
}
async function changeMap(file){
  await loadMap(file);
}
function input(event){
  player.tempX=player.x;
  player.tempY=player.y;
  //console.log(event.code);
  switch(event.code) {
    case "KeyW":
      if(player.y>0&&(player.dir==dir.UP||shift==true)){
        player.tempY--;
      }
      if(!shift&&!(mode==modeType.EDIT)){player.dir=dir.UP;}
      break;
    case "KeyA":
      if(player.x>0&&(player.dir==dir.LEFT||shift==true)){
        player.tempX--
      }
      if(!shift&&!(mode==modeType.EDIT)){player.dir=dir.LEFT;}
      break;
    case "KeyS":
      if(player.y<map.length-1&&(player.dir==dir.DOWN||shift==true)){
        player.tempY++
      }
      if(!shift&&!(mode==modeType.EDIT)){player.dir=dir.DOWN;}
      break;
    case "KeyD":
      if(player.x<map[player.y].length-1&&(player.dir==dir.RIGHT||shift==true)){
        player.tempX++;
      }
      if(!shift&&!(mode==modeType.EDIT)){player.dir=dir.RIGHT;}
      break;
    case "KeyE":
      interact();
      break;
    case "KeyK":
      if(devMode){
        hit = raycast(player.x,player.y,player.dir,12);
        if(hit.length==1){
          enemies[hit].health=0;
        }
      }
    case "Backquote":
      if(devMode){
        if(mode==modeType.NORMAL){mode=1}else if(mode==modeType.TARGET){mode=2}else{mode=0};break;
      }
    case "Enter":
      mapOutput();
      break;
    case "Equal":
      if(devMode){
        changeMap("map1");
      }
      break;
    case "NumpadAdd":
      castSpell();
      break;
    case "Numpad0":
      if(mode==modeType.TARGET){
        //castSelf()
      }else if(mode==modeType.EDIT){
        map[player.y][player.x].cType=cellType.WALL;}break;
    case "Numpad1":
      if(mode==modeType.NORMAL){
        spell=spells.LIFE;
        mode=1;
      }else if(mode==modeType.EDIT){
        map[player.y][player.x].cType=cellType.FLOOR;
      }break;
    case "Numpad2":
      if(mode==modeType.NORMAL){
        spell=spells.WATER;
        mode=1;
      }else if(mode==modeType.EDIT){
        map[player.y][player.x].cType=cellType.DOOR;
        map[player.y][player.x].state=properties.CLOSED;
      }break;
    case "Numpad3":
      if(mode==modeType.NORMAL){
        spell=spells.ICE;
        mode=1;
      }else if(mode==modeType.EDIT){
        map[player.y][player.x].cType=cellType.DOOR;
        map[player.y][player.x].state=properties.OPEN;
      }break;
    case "Numpad4":
      if(mode==modeType.NORMAL){
        spell=spells.EARTH;
        mode=1;
      }else if(mode==modeType.EDIT){
        map[player.y][player.x].cType=cellType.LOAD;
        map[player.y][player.x].file="";
        map[player.y][player.x].startX=1;
        map[player.y][player.x].startY=1;
      }break;
    case "Numpad5":
      if(mode==modeType.NORMAL){
      }else if(mode==modeType.EDIT){
        map[player.y][player.x].cType=cellType.GOLD;
      }break;
    case "Numpad6":
      if(mode==modeType.NORMAL){
        spell=spells.AIR;
        mode=1;
      }else if(mode==modeType.EDIT){
      }break;
    case "Numpad7":
      if(mode==modeType.NORMAL){
        spell=spells.LAVA;
        mode=1;
      }else if(mode==modeType.EDIT){
      }break;
    case "Numpad8":
      if(mode==modeType.NORMAL){
        spell=spells.FIRE;
        mode=1;
      }else if(mode==modeType.EDIT){
      }break;
    case "Numpad9":
      if(mode==modeType.NORMAL){
        spell=spells.FIREBALL;
        mode=1;
      }else if(mode==modeType.EDIT){
      }break;
    case "NumpadSubtract":
    case "NumpadEnter":
      spell=spells.NONE;
      mode=0;
      break;
    case "KeyI":
      console.log(enemies[raycast(player.x,player.y,player.dir)]);
      break;
    case "KeyU":
      update();
      break;
    case "ShiftLeft":
        shift = true;
  }
  if((checkMove(player.tempX,player.tempY)&&!mode==modeType.TARGET)||mode==modeType.EDIT){
    player.x=player.tempX;
    player.y=player.tempY;
    update();
  }
  drawMap();
}
function hitSounds(){
  if(hitCount>0){
    var audio = new Audio('hitSound.mp3');
    audio.play();
    hitCount--;
    setTimeout(hitSounds,75);
  }
}

function update(){
  time++;
  if(map[player.y][player.x].cType==cellType.LOAD){
    changeMap(map[player.y][player.x].file);
  }
  if(player.status==statusList.NONE){
    player.duration=0;
  }
  enemies.forEach(function(enemy){
    if(enemy.duration>0){
      enemy.duration--;
      if(enemy.status==statusList.FLAMING){
        enemy.health--;
      }
    }
    if(enemy.duration<=0&&!enemy.status==statusList.NONE){
      if(enemy.status==statusList.FROZEN){
        enemy.status=statusList.WET;
        enemy.duration=durationList.WET;
      }else{
        enemy.status=statusList.NONE;
      }
    }

  })
  //remove dead enemies from array
  var enemyCount = enemies.length;
  enemies = enemies.filter(function(item){return item.health > 0;});
  score += enemyCount-enemies.length;//add one score for each enemy defeated
  //attack player
  hitCount=locateAdjacent(player.x,player.y,true).length;
  if(mode!=2){
    player.health-=hitCount;
    hitSounds();
  }

  for(var i=0;i<enemies.length;i++){
    enemies[i].tempX=enemies[i].x;
    enemies[i].tempY=enemies[i].y;
    if(!(enemies[i].status==statusList.FROZEN||enemies[i].status==statusList.STUCK)){
      if(Math.abs(player.x-enemies[i].x)>Math.abs(player.y-enemies[i].y)){//further x or y check
        if(player.x-enemies[i].x>0){//which direction x
          enemies[i].tempX=enemies[i].x+1;
          enemies[i].dir=dir.RIGHT;
        }else if(player.x-enemies[i].x<0){
         enemies[i].tempX=enemies[i].x-1;
          enemies[i].dir=dir.LEFT;
        }
      }else{
        if(player.y-enemies[i].y>0){//which direction y
          enemies[i].tempY=enemies[i].y+1;
          enemies[i].dir=dir.DOWN;
        }else if(player.y-enemies[i].y<0){
          enemies[i].tempY=enemies[i].y-1;
          enemies[i].dir=dir.UP;
        }
      }
      if(checkMove(enemies[i].tempX,enemies[i].tempY)){//move if able
        if(time%2){
          enemies[i].x=enemies[i].tempX;
          enemies[i].y=enemies[i].tempY;
          enemies[i].dir=0;
        }
      }else{
        enemies[i].tempX=enemies[i].x;
        enemies[i].tempY=enemies[i].y;
        if(Math.abs(player.x-enemies[i].x)<Math.abs(player.y-enemies[i].y)){//other direction if not able to move
          if(player.x-enemies[i].x>0){//which direction x
            enemies[i].tempX=enemies[i].x+1;
            enemies[i].dir=dir.RIGHT;
          }else if(player.x-enemies[i].x<0){
            enemies[i].tempX=enemies[i].x-1;
            enemies[i].dir=dir.LEFT;
          }
        }else{
          if(player.y-enemies[i].y>0){//which direction y
            enemies[i].tempY=enemies[i].y+1;
            enemies[i].dir=dir.DOWN;
          }else if(player.y-enemies[i].y<0){
            enemies[i].tempY=enemies[i].y-1;
            enemies[i].dir=dir.UP;
          }
        }
        if(checkMove(enemies[i].tempX,enemies[i].tempY)){
          if(time%2){
            enemies[i].x=enemies[i].tempX;
            enemies[i].y=enemies[i].tempY;
            enemies[i].dir=0;
          }
        }
      }
    }
  }
  if(player.health<1){
    location.reload();
  }
}
 
function checkMove(x,y){//return bol weather space can be moved to
  var cell=map[y][x];
  if(locateEnemy(x,y)>-1||(x==player.x&&y==player.y)){return false}
  if(cell.state==properties.OPEN){return true}
  return moveable[cell.cType]
}

function drawMap(){
  ctx.fillStyle = background;
  ctx.fillRect(0,0,canvas1.width,canvas1.height)
  var fillColor;
  var cell;
  for(var i=0;i<map.length;i++){
    for(var j=0;j<map[i].length;j++){
      cell = map[i][j];
      fillColor = statusPalette[cell.cType];
      if(cell.state==properties.OPEN){
        fillColor = statusPalette[cellType.FLOOR];
      }
      //console.log("i="+i+" j="+j+" cell="+map[j][i]+" color="+palette[map[j][i]]);
      box(j,i,palette[cell.cType],fillColor,7)
    }
  }
  drawEntities();
  drawSpell();
  document.getElementById("score").innerHTML=score;
}

function box(x,y,color="#000000",fill="#999999",round=10){
  var curveSize=round;
  var radQ = 0.5*Math.PI;
  ctx.fillStyle = fill;
  ctx.fillRect(x*boxSize+boxSpace+(ctx.lineWidth/2),y*boxSize+boxSpace+(ctx.lineWidth/2),boxSize-boxSpace-ctx.lineWidth,boxSize-boxSpace-ctx.lineWidth);
  ctx.beginPath();
  ctx.strokeStyle = color;
  ctx.arc((x+1)*boxSize-curveSize, (y+1)*boxSize-curveSize, curveSize, 0, radQ);  //lower right
  ctx.arc(x*boxSize+curveSize+boxSpace, (y+1)*boxSize-curveSize, curveSize, radQ, 2*radQ); //lower left
  ctx.arc(x*boxSize+curveSize+boxSpace, y*boxSize+curveSize+boxSpace, curveSize, 2*radQ, 3*radQ);   //upper left
  ctx.arc((x+1)*boxSize-curveSize, y*boxSize+curveSize+boxSpace, curveSize, 3*radQ, 0);    //upper right
  ctx.arc((x+1)*boxSize-curveSize, (y+1)*boxSize-curveSize, curveSize, 0, radQ);  //lower right
  ctx.stroke();
}

function drawEntities(){
  var borderColor = (player.status == statusList.NONE) ? player.borderColor : statusColors[player.status];

  circle(player.x,player.y,0.7,borderColor);
  drawHealth(player.x,player.y,0.50,player.fillColor,player.health,player.maxHealth)
  if(player.dir){drawArrow(player.x,player.y,player.dir,player.borderColor);}

  enemies.forEach(function(enemy){
    borderColor = (enemy.status == statusList.NONE) ? enemy.borderColor : statusColors[enemy.status];
    circle(enemy.x,enemy.y,0.7,borderColor);
    drawHealth(enemy.x,enemy.y,0.55,enemy.fillColor,enemy.health,enemy.maxHealth);
    if(enemy.dir){
      drawArrow(enemy.x,enemy.y,enemy.dir,enemy.borderColor);
    }      
  });
}

function drawHealth(x,y,size,fillColor,health,maxHealth=5){
  var healthPct = health/maxHealth;
  var end = (1.5-(0.9*(1-healthPct)))*Math.PI;
  var start = (1.51+(0.9*(1-healthPct)))*Math.PI;
  ctx.beginPath();
  ctx.arc(center(x),center(y),size*(boxSize-boxSpace)/2,start,end);
  ctx.fillStyle = fillColor;
  ctx.fill();
  
}

function interact(){
  var interact;
  var x=player.x;
  var y=player.y;
  switch(player.dir){
    case 1:
      y--;
      break;
    case 2:
      x++;
      break;
    case 3:
      y++;
      break;
    case 4:
      x--;
      break;
  }
  interact=map[y][x]
  switch(interact.cType){
    case cellType.DOOR:
      if(interact.state==properties.CLOSED){
        map[y][x].state=properties.OPEN;
      }else{
        map[y][x].state=properties.CLOSED;
      }
      break;
    case cellType.GOLD:
      map[y][x].cType=cellType.FLOOR;
      score+=5;
      var audio = new Audio('coinSound.mp3');
      audio.play();
      break;
  }
}

function locateEnemy(x,y){
  for(var i=0;i<enemies.length;i++){
    if(enemies[i].x==x&&enemies[i].y==y){
      return i
    }
  }
  return -1
}
function locateAdjacent(x,y,carindal=false){
  var idx;
  var output=[];
  if(!carindal){
    if(locateEnemy(x-1,y-1)!=-1){output.push(locateEnemy(x-1,y-1));}
    if(locateEnemy(x+1,y-1)!=-1){output.push(locateEnemy(x+1,y-1));}
    if(locateEnemy(x-1,y+1)!=-1){output.push(locateEnemy(x-1,y+1));}
    if(locateEnemy(x+1,y+1)!=-1){output.push(locateEnemy(x+1,y+1));}
  }
  if(locateEnemy(x  ,y-1)!=-1){output.push(locateEnemy(x  ,y-1));}
  if(locateEnemy(x-1,y  )!=-1){output.push(locateEnemy(x-1,y));}
  if(locateEnemy(x+1,y  )!=-1){output.push(locateEnemy(x+1,y));}
  if(locateEnemy(x  ,y+1)!=-1){output.push(locateEnemy(x  ,y+1));}
  return output;
}
function getType(x,y){
  if(y<=map.length&&y>=0){
    if(x<map[y].length&&x>=0){
      return map[y][x].cType;
    }else{
      return cellType.WALL;
    }
  }else{
    return cellType.WALL;
  }
}
function raycast(x,y,dir,maxDist=20){
  var deltaX=0;
  var deltaY=0;
  var rayHit;
  var check=-1;
  var dist=0;
  switch(dir){
    case 1:
      deltaY=-1;
      break;
    case 2:
      deltaX=1;
      break;
    case 3:
      deltaY=1;
      break;
    case 4:
      deltaX=-1;
      break;
  }
  while(check==-1&&!rayHit){
    x+=deltaX;
    y+=deltaY;
    dist++;
    check=locateEnemy(x,y);
    if(getType(x,y)==cellType.WALL||maxDist<=dist){
      rayHit=1;
      x-=deltaX;
      y-=deltaY;
    }
  }
  if(rayHit){
    return [x,y]
  }else{return [check]}
}

function splash(x,y,dir){
  var hit = raycast(x,y,dir);
  var hitX;
  var hitY;
  var output;
  if (hit.length==1){
    hitX = enemies[hit[0]].x;
    hitY = enemies[hit[0]].y;
  }else{
    hitX = hit[0];
    hitY = hit[1];
  }
  output=locateAdjacent(hitX,hitY)
  if (hit.length==1){
  output.push(hit[0]);
  }
  return output;
}

function castSpell(){
  switch(spell){
    case spells.LIFE:
      player.health+=5;
      if(player.health>player.maxHealth){player.health=player.maxHealth;}
      // locateAdjacent(player.x,player.y).forEach(function(enemy){
      //   enemy.health+=1;
      // })
      break;
    case spells.WATER:
      splash(player.x,player.y,player.dir).forEach(function(enemy){
        if(enemies[enemy].status==statusList.FROZEN){
          enemies[enemy].health--;
        }else if(enemies[enemy].status==statusList.FLAMING){
          enemies[enemy].status=statusList.NONE;
          enemies[enemy].duration=0;
        }else{
          enemies[enemy].status=statusList.WET;
          enemies[enemy].duration=durationList.WET;
        }
      });
      break;
    case spells.ICE:
      var hit = raycast(player.x,player.y,player.dir);
      if (hit.length==1){
        if(enemies[hit[0]].status==statusList.WET){
          enemies[hit[0]].health-=3;
          enemies[hit[0]].status=statusList.FROZEN;
          enemies[hit[0]].duration=durationList.FROZEN;
        }else if(enemies[hit[0]].status==statusList.FLAMING){
          enemies[enemy].status=statusList.WET;
          enemies[enemy].duration=Math.floor(durationList.WET/3);
        }else{
          enemies[hit[0]].status=statusList.FROZEN;
          enemies[hit[0]].duration=durationList.FROZEN;
        }
      }
    case spells.EARTH:
      var hit = raycast(player.x,player.y,player.dir);
      if (hit.length==1){
        enemies[hit[0]].health-=3
      }
      break;
    case spells.AIR:
      var hit = raycast(player.x,player.y,player.dir,5);
      if (hit.length==1){
        var location = raycast(enemies[hit[0]].x,enemies[hit[0]].y,player.dir,4);
        if(location.length==2){
        enemies[hit[0]].x=location[0];
        enemies[hit[0]].y=location[1];
        }else{
          enemies[hit[0]].x=enemies[location[0]].x;
          enemies[hit[0]].y=enemies[location[0]].y;
          switch(player.dir){
            case 1:
              enemies[hit[0]].y++;
              break;
            case 2:
              enemies[hit[0]].x--;
              break;
            case 3:
              enemies[hit[0]].y--;
              break;
            case 4:
              enemies[hit[0]].x++;
              break;
          }
        }
      }
      break;
    case spells.LAVA:
      var hit = raycast(player.x,player.y,player.dir);
      if (hit.length==1){//if lava hits enemy
        enemies[hit[0]].health-=2;
        if(enemies[hit[0]].status==statusList.FROZEN){
          enemies[hit[0]].status=statusList.WET;
          enemies[hit[0]].duration=Math.floor(durationList.WET/3);
        }else if(enemies[hit[0]].status==statusList.WET){
          enemies[hit[0]].status=statusList.NONE;
          enemies[hit[0]].duration=0;
        }else{
          enemies[hit[0]].status=statusList.FLAMING;
          enemies[hit[0]].duration=durationList.FLAMING;
        }
      }else{//if lava hits wall
        locateAdjacent(hit[0],hit[1]).forEach(function(enemy){
          if(enemy.status==statusList.FROZEN){
            enemy.status=statusList.WET;
            enemy.duration=Math.floor(durationList.WET/3);
          }else if(enemy.status==statusList.WET){
            enemy.status=statusList.NONE;
            enemy.duration=0;
          }else{
            enemy.status=statusList.FLAMING;
            enemy.duration=durationList.FLAMING;
          }
        })
      }
      break;
    case spells.FIRE:
      var hit = raycast(player.x,player.y,player.dir);
      if (hit.length==1){
        if(enemies[hit[0]].status==statusList.FROZEN){
          enemies[hit[0]].status=statusList.WET;
          enemies[hit[0]].duration=Math.floor(durationList.WET);
        }else if(enemies[hit[0]].status==statusList.WET){
          enemies[hit[0]].status=statusList.NONE;
          enemies[hit[0]].duration=0;
          enemies[hit[0]].health-=1;
        }else{
          enemies[hit[0]].status=statusList.FLAMING;
          enemies[hit[0]].duration=durationList.FLAMING;
          enemies[hit[0]].health-=2;
        }
      }
      break;
    case spells.FIREBALL:
      splash(player.x,player.y,player.dir).forEach(function(enemy){
        enemies[enemy].health-=1;
        if(enemies[enemy].status==statusList.FROZEN){
          enemies[enemy].status=statusList.WET;
          enemies[enemy].duration=Math.floor(durationList.WET);
        }else if(enemies[enemy].status==statusList.WET){
          enemies[enemy].status=statusList.NONE;
          enemies[enemy].duration=0;
        }else{
          enemies[enemy].status=statusList.FLAMING;
          enemies[enemy].duration=durationList.FLAMING;
        }
      });
      break;

  }
  mode=0;
  spell=spells.NONE;
  update();
}

function drawSpell() {
  ctx.strokeStyle = spellColors[spell];
  ctx.beginPath();
  ctx.arc(center(2),center(0),0.75*(boxSize-boxSpace)/2,Math.PI/2+Math.PI/4,1.5*Math.PI+(Math.PI/4));
  ctx.fillStyle = spellColors[spell];
  ctx.fill();
  ctx.stroke();
  for(key in spells){
    if (spells[key] == spell){
      document.getElementById("spellDisplay").innerHTML=key;
    }
  }
  
}

function mapOutput(){
  var output="[";
  for(i=0;i<map.length;i++){
    output+="["
    for(j=0;j<map[i].length;j++){
      output+="{"+{
        0:"cType:cellType.WALL",
        1:"cType:cellType.FLOOR",
        2:"cType:cellType.DOOR,state:properties.CLOSED",
        3:"cType:cellType.DOOR,state:properties.OPEN"
      }[map[i][j].cType]+"},";
    }
    output=output.substring(0,output.length-1);
    output+="],"
  }
  output=output.substring(0,output.length-2);
  output+="]"
   console.log(output);
}
function center(cell){
  return (cell+0.5)*boxSize+(boxSpace/2);
}
function circle(x,y,size,color="#555577",fillColor){
  ctx.strokeStyle = color;
  ctx.beginPath();
  ctx.arc(center(x),center(y),size*(boxSize-boxSpace)/2,0,2*Math.PI);
  if(fillColor){
    ctx.fillStyle = fillColor;
    ctx.fill();
  }
  ctx.stroke();
}

function drawArrow(x,y,dir,color="#000000") {
  //circle(x*boxSize+boxSpace,y*boxSize+boxSpace,0.5,"#FF0000");
  ctx.strokeStyle = color;
  var tempx;
  var tempy
  switch(dir){
    case 1:
      y-=0.2;
      break;
    case 2:
      x+=0.2;
      break;
    case 3:
      y+=0.2;
      break;
    case 4:
      x-=0.2;
      break;
  }
  ctx.beginPath();
  ctx.moveTo(center(x),center(y));
  switch(dir){
    case 1:
      y-=0.65;
      break;
    case 2:
      x+=0.65;
      break;
    case 3:
      y+=0.65;
      break;
    case 4:
      x-=0.65;
      break;
  }
  tempx=x;
  tempy=y;
  ctx.lineTo(center(x),center(y));
  switch(dir){
    case 1:
      y+=0.15;
      x-=0.15;
      break;
    case 2:
      x-=0.15;
      y+=0.15;
      break;
    case 3:
      y-=0.15;
      x+=0.15;
      break;
    case 4:
      x+=0.15;
      y-=0.15;
      break;
  }
  ctx.lineTo(center(x),center(y));
  x=tempx;
  y=tempy;
  ctx.moveTo(center(x),center(y));
  switch(dir){
    case 1:
      y+=0.15;
      x+=0.15;
      break;
    case 2:
      x-=0.15;
      y-=0.15;
      break;
    case 3:
      y-=0.15;
      x-=0.15;
      break;
    case 4:
      x+=0.15;
      y+=0.15;
      break;
  }
  ctx.lineTo(center(x),center(y));
  ctx.stroke();
  circle(x*boxSize+boxSpace,y*boxSize+boxSpace,0.5,"#0000FF");
}